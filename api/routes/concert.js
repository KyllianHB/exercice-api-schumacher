var express = require('express');
var router = express.Router();
var connection = require('../db');


router.get('/concerts', function (req, res, next) {
    // #swagger.summary = "Liste de tous les concerts"
    
    var query = `SELECT * FROM Concert`
    
    connection.query(query, (error, rows, fields) => {
        
        const domain =  req.protocol + '://' + req.get('host')
        console.log('BASEURL : '+req.baseUrl)


      const concerts = rows.map(element => {
        return {
            _link: {
                self: {
                  href: `${domain}/concerts/${element.id}`,
                  templated: false
                },
                next: {
                  href: `${domain}/concerts/${(element.id)+1}`
                },
                reservation: {
                  href: `${domain}/concerts/${element.id}/reservation`,
                  templated: false
                }
              },
              _embeded: {
                  description: element.description,
                  date: element.date,
                  artiste: element.artiste,
                  lieu: element.lieu,
                  nb_places: element.nb_places
              }
        }
      });


      const response = {
        _link: `${domain}/concerts`,
        _embeded : {
            concerts : concerts
        }
      }

      res.status(200).send(response)
    })
  
  });

/* GET home page. */
router.get('/concerts/:id', function (req, res, next) {
    // #swagger.summary = "Liste de tous les concerts"
    const domain =  req.protocol + '://' + req.get('host')

    var query = `SELECT * FROM Concert WHERE id = ${[req.params.id]}`
    connection.query(query, (error, rows, fields) => {

        if(error) {
            res.send("Error server").status(500)
            console.log(error)
        } else {
            const concert = rows.map(element => {
                return {
                    _link: {
                      self: {
                        href: `${domain}/concerts/${element.id}`,
                        templated: false
                      },
                      next: {
                        href: `${domain}/concerts/${(element.id)+1}`
                      },
                      reservation: {
                        href: `${domain}/concerts/${element.id}/reservation`,
                        templated: false
                      }
                    },
                    _embeded: {
                        description: element.description,
                        date: element.date,
                        artiste: element.artiste,
                        lieu: element.lieu,
                        nb_places: element.nb_places
                    }
                }
              });
        
        
              const response = {
                _link: {
                    self: {
                        href: `${domain}/concerts`
                    }
                },
                _embeded : {
                    concerts : concert
                }
              }
            if(concert == '') res.status(400).send('No values')
            else if (concert == null) res.status(500).send(error)
            else res.set('Content-Type','application/json').status(200).send(response)
        }
    })
})

router.post('/concerts/:id/reservations', function (req, res, next) {    
    
    /* #swagger.parameters['pseudo'] = {
    in: 'formData',
    description: 'Le pseudo de l\'utilisateur qui effectue la réservation',
    required: 'true',
    type: 'string',
    format: 'application/x-www-form-urlencoded',
    } */

    const reservation = {
            pseudo: req.body.pseudo
        }
    
    var query = `SELECT SUM(
        (select nb_places from concert where id = ${[req.params.id]}) - 
        (select count(*) from reservation where statut != 'annulée' AND concert_id = ${[req.params.id]})
        ) as nb_places_dispo`
    connection.query(query, (error, rows, fields) => {
        if(error) {
            res.status(500).send("Error server")
            console.log(error)
        } else {
            res.set('Content-Type','application/json').status(201).send(reservation)
        }
    
    })

    var query = `INSERT INTO reservation (user_id, concert_id, statut, date_reservation) VALUES ((SELECT id FROM user where pseudo = "${[reservation.pseudo]}"), ${[req.params.id]}, "confirmée", NOW());`
    console.log(query)
    connection.query(query, (error, rows, fields) => {
        if(error) {
            res.status(500).send("Error server")
            console.log(error)
        } else {
            res.set('Content-Type','application/json').status(201).send(reservation)
        }
    
    })
});
  
  module.exports = router;
